package pl.jwai.score;

import pl.jwai.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class Score {

    private int points;
    private List<Pair<Integer, String>> history;

    public Score() {
        points = 0;
        history = new ArrayList<>();
    }

    public void addPointsForGesture(int points, String gesture) {
        this.points += points;
        history.add(new Pair<>(points, gesture));
    }

    public int getPoints() {
        return points;
    }

    public List<Pair<Integer, String>> getHistory() {
        return history;
    }
}
