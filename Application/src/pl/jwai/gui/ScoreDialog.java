package pl.jwai.gui;

import pl.jwai.score.Score;
import pl.jwai.util.Pair;

import javax.swing.*;
import java.awt.*;

public class ScoreDialog extends JDialog {

    private Score score;

    public ScoreDialog(JFrame parent, Score score) {
        super(parent, "Wynik");

        this.score = score;

        this.setTitle("Wynik");
        this.setModal(true);
        this.setSize(new Dimension(200, 250));
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.setLocationRelativeTo(parent);

        JPanel panel = (JPanel) this.getContentPane();
        panel.setLayout(new BorderLayout());

        JTextArea area = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(area);

        area.append("Zdobyles " + score.getPoints() + " punktow! :)\n");
        for (Pair<Integer, String> entry : score.getHistory()) {
            area.append(entry.getSecond() + ", +" + entry.getFirst() + "pkt\n");
        }

        panel.add(scrollPane, BorderLayout.CENTER);

        JButton button = new JButton("Zamknij program");
        button.addActionListener(event -> {
            ScoreDialog.this.dispose();
        });
        panel.add(button, BorderLayout.SOUTH);
    }

}
