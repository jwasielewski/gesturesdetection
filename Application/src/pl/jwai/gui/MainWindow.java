package pl.jwai.gui;

import com.google.common.eventbus.Subscribe;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwai.bus.Events;
import pl.jwai.bus.GDEventBus;
import pl.jwai.models.JsonAppConfiguration;
import pl.jwai.models.JsonScoreEntry;
import pl.jwai.neuralnetwork.NeuralNetwork;
import pl.jwai.score.Score;
import pl.jwai.util.GestureListener;
import pl.jwai.util.RoundTimer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

public class MainWindow extends JFrame {
    private static final Logger log = LoggerFactory.getLogger(MainWindow.class);

    private NeuralNetwork neuralNetwork;
    private JsonAppConfiguration configuration;
    private Score score;
    private RoundTimer roundTimer;

    private JLabel points;

    public MainWindow(JsonAppConfiguration configuration) {
        this.configuration = configuration;
        this.score = new Score();

        neuralNetwork = new NeuralNetwork();
        neuralNetwork.readFromFile("neuralnetwork.json");

        this.setTitle("GesturesDetection");
        this.setSize(new Dimension(800, 600));
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);

        GDEventBus.instance().register(this);

        createGUI();
    }

    private void createGUI() {
        GesturePanel gesturePanel = new GesturePanel();
        gesturePanel.setEnabled(false);
        JPanel controlsPanel = new JPanel(new GridLayout(20, 1));
        controlsPanel.setPreferredSize(new Dimension(250, getHeight()));

        JPanel mainPanel = (JPanel) getContentPane();
        mainPanel.setLayout(new BorderLayout());

        JButton startRound = new JButton("Rozpocznij gre");
        startRound.addActionListener(event -> {
            startRound.setEnabled(false);
            roundTimer = new RoundTimer(configuration.getRoundTime());
            roundTimer.run();
            gesturePanel.setEnabled(true);
        });
        controlsPanel.add(startRound);

        points = new JLabel("Punkty: 0");
        controlsPanel.add(points);

        JLabel gestureStatus = new JLabel("Status: idle");
        GestureListener listener = new GestureListener() {
            @Override
            public void onGestureStart() {
                gestureStatus.setText("Status: wprowadzanie gestu");
            }

            @Override
            public void onGestureEnd() {
                gestureStatus.setText("Status: zakonczono wprowadzanie gestu");
                neuralNetwork.setInputs(gesturePanel.getInputForNeuralNetwork());
                addPointsForGesture(checkNeuralNetworkOutput(neuralNetwork.getOutput()));
                points.setText("Punkty: " + score.getPoints());
                gestureStatus.setText("Status: dodano punkty");
                gesturePanel.clear();
            }
        };
        gesturePanel.setGestureListener(listener);
        controlsPanel.add(gestureStatus);

        JButton clearButton = new JButton("Wyczysc");
        clearButton.addActionListener(event -> {
            gesturePanel.clear();
            gestureStatus.setText("Status: idle");
        });
        controlsPanel.add(clearButton);

        JCheckBox setGesturePanelDebug = new JCheckBox("Wlacz debugowanie");
        setGesturePanelDebug.addChangeListener(event -> {
            if (setGesturePanelDebug.isSelected()) {
                setGesturePanelDebug.setText("Wylacz debugowanie");
            } else {
                setGesturePanelDebug.setText("Wlacz debugowanie");
            }

            gesturePanel.setDebug(setGesturePanelDebug.isSelected());
        });
        controlsPanel.add(setGesturePanelDebug);

        mainPanel.add(gesturePanel, BorderLayout.CENTER);
        mainPanel.add(controlsPanel, BorderLayout.EAST);
    }

    @Subscribe
    public void onEvent(Events event) {
        log.debug("EVENT: '{}'", event);
        if (event == Events.endOfRound) {
            if (roundTimer != null) {
                roundTimer.shutdown();
            }
            ScoreDialog dialog = new ScoreDialog(MainWindow.this, score);
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    super.windowClosed(e);
                    shutdown();
                }
            });
            dialog.setVisible(true);
        }
    }

    private int checkNeuralNetworkOutput(double[] output) {
        int[] result = new int[output.length];
        for (int i = 0; i < result.length; ++i) {
            result[i] = 0;
        }

        int maxIndex = 0;
        double maxValue = output[0];
        for (int i = 1; i < output.length; ++i) {
            if (output[i] > maxValue) {
                maxValue = output[i];
                maxIndex = i;
            }
        }

        if (output[maxIndex] < 0.45) {
            return -1;
        }

        result[maxIndex] = 1;

        log.debug("Result: {}", Arrays.toString(result));
        log.debug("Output: {}", Arrays.toString(output));

        return maxIndex + 1;
    }

    private void addPointsForGesture(int gestureId) {
        for (JsonScoreEntry scoreEntry : configuration.getGestures()) {
            if (scoreEntry.getId() == gestureId) {
                score.addPointsForGesture(scoreEntry.getPoints(), scoreEntry.getName());
                return;
            }
        }
        score.addPointsForGesture(0, "Nieznany gest");
    }

    private void shutdown() {
        this.setVisible(false);
        System.exit(0);
    }
}
