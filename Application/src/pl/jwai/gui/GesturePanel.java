package pl.jwai.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwai.util.GestureListener;
import pl.jwai.util.Pair;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.*;
import java.util.List;

public class GesturePanel extends JPanel implements MouseMotionListener, MouseListener {
    private static final Logger log = LoggerFactory.getLogger(GesturePanel.class);

    private static final int CELLS_IN_HORIZONTAL = 8;
    private static final int CELLS_IN_VERTICAL = 8;

    private Map<String, Pair<Integer, Integer>> mMap;
    private List<Point> mLine;

    private double[] inputForNeuralNetwork;

    private boolean isDebug;

    private GestureListener mListener;

    public GesturePanel() {
        initialize();
    }

    private void initialize() {
        mMap = new HashMap<>();
        mLine = new LinkedList<>();

        isDebug = false;

        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addMouseMotionListener(this);
        this.addMouseListener(this);

        clearInputForNeuralNetwork();
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;

        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHints(rh);

        g2d.clearRect(0, 0, getWidth(), getHeight());

        if (isEnabled()) {
            int cellWidth = getWidth() / CELLS_IN_HORIZONTAL;
            int cellHeight = getHeight() / CELLS_IN_VERTICAL;

            int spaceh = (getWidth() - cellWidth * CELLS_IN_HORIZONTAL) / 2;
            int spacev = (getHeight() - cellHeight * CELLS_IN_VERTICAL) / 2;

            if (isDebug) {
                g2d.drawLine(spaceh, spacev, getWidth() - spaceh, spacev);
                g2d.drawLine(spaceh, getHeight() - spacev, getWidth() - spaceh, getHeight() - spacev);
                g2d.drawLine(spaceh, spacev, spaceh, getHeight() - spacev);
                g2d.drawLine(getWidth() - spaceh, spacev, getWidth() - spaceh, getHeight() - spacev);
            }

            if (mMap.size() > 0 && isDebug) {
                g2d.setColor(Color.BLACK);
                for (Map.Entry<String, Pair<Integer, Integer>> entry : mMap.entrySet()) {
                    g2d.fillRect(spaceh + entry.getValue().getFirst() * cellWidth, spacev + entry.getValue().getSecond()
                            * cellHeight, cellWidth, cellHeight);
                }
            }

            if (isDebug) {
                g2d.setColor(Color.BLACK);
                for (int i = 0; i <= CELLS_IN_HORIZONTAL; i++) {
                    g2d.drawLine(spaceh + i * cellWidth, spacev, spaceh + i * cellWidth, spacev + cellHeight
                            * CELLS_IN_VERTICAL);
                }

                for (int i = 0; i <= CELLS_IN_VERTICAL; i++) {
                    g2d.drawLine(spaceh, spacev + i * cellHeight, spaceh + cellWidth * CELLS_IN_VERTICAL, spacev + i
                            * cellHeight);
                }
            }
            g2d.setColor(Color.BLUE);
            BasicStroke stroke = new BasicStroke(8, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL);
            g2d.setStroke(stroke);

            if (!isDebug && mLine.size() > 2) {
                for (int i = 0; i < mLine.size() - 1; ++i) {
                    g2d.drawLine(mLine.get(i).x, mLine.get(i).y, mLine.get(i + 1).x, mLine.get(i + 1).y);
                }
            }

            g2d.setColor(Color.BLACK);
        } else {
            g2d.setColor(Color.RED);

            g2d.drawLine(0, 0, getWidth(), getHeight());
            g2d.drawLine(0, getHeight(), getWidth(), 0);
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (!isEnabled()) {
            return;
        }

        calculateCoordsOfChoosenRectangle(e.getPoint());
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        //log.debug("MousePressed at: '{}'", e.getPoint());
        if (!isEnabled()) {
            return;
        }

        clear();
        if (mListener != null) {
            mListener.onGestureStart();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //log.debug("MouseReleased at: '{}'", e.getPoint());
        if (!isEnabled()) {
            return;
        }

        if (isDebug) {
            printInputsForNeuralNetworks();
        }

        if (mListener != null) {
            mListener.onGestureEnd();
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public void setDebug(boolean debug) {
        isDebug = debug;
        repaint();
    }

    public void clear() {
        mLine.clear();
        mMap.clear();
        clearInputForNeuralNetwork();
        repaint();
    }

    public void setGestureListener(GestureListener listener) {
        mListener = listener;
    }

    private void calculateCoordsOfChoosenRectangle(Point point) {
        try {
            int cellWidth = getWidth() / CELLS_IN_HORIZONTAL;
            int cellHeight = getHeight() / CELLS_IN_VERTICAL;

            int spaceh = (getWidth() - cellWidth * CELLS_IN_HORIZONTAL) / 2;
            int spacev = (getHeight() - cellHeight * CELLS_IN_VERTICAL) / 2;

            if (point.x >= spaceh + cellWidth * CELLS_IN_HORIZONTAL
                    || point.y >= spacev + cellHeight * CELLS_IN_VERTICAL) {
                return;
            } else if (point.x <= spaceh || point.y <= spacev) {
                return;
            }

            int x1 = (point.x - spaceh) / cellWidth;
            int y1 = (point.y - spacev) / cellHeight;

            if (!mMap.containsKey(getRectangleName(x1, y1))) {
                mMap.put(getRectangleName(x1, y1), new Pair<>(x1, y1));
                inputForNeuralNetwork[y1 * CELLS_IN_HORIZONTAL + x1] = 0.8;
//                if(isDebug) {
//                    log.debug("Inserting '1' at position '{}'", y1 * CELLS_IN_HORIZONTAL + x1);
//                }
            }

            mLine.add(point);

            repaint();
        } catch (Throwable t) {
            System.err.println(t.getMessage());
        }
    }

    public double[] getInputForNeuralNetwork() {
        return inputForNeuralNetwork;
    }

    private void clearInputForNeuralNetwork() {
        inputForNeuralNetwork = new double[CELLS_IN_HORIZONTAL * CELLS_IN_VERTICAL];
        for (int i = 0; i < inputForNeuralNetwork.length; ++i) {
            inputForNeuralNetwork[i] = -0.8;
        }
    }

    private void printInputsForNeuralNetworks() {
//        for(int i = 0; i < CELLS_IN_VERTICAL; ++i) {
//            for(int j = 0; j < CELLS_IN_HORIZONTAL; ++j) {
//                System.out.print(String.format("%d,", inputForNeuralNetwork[i * CELLS_IN_HORIZONTAL + j]));
//            }
//            System.out.print("\n");
//        }
        System.out.print(Arrays.toString(inputForNeuralNetwork));
        System.out.print("\n");
    }

    private String getRectangleName(int x, int y) {
        return String.format("(X:%d,Y:%d)", x, y);
    }
}
