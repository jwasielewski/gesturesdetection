package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JsonAppConfiguration {

    @SerializedName("round_time")
    @Expose
    private int roundTime;
    @SerializedName("gestures")
    @Expose
    private List<JsonScoreEntry> gestures = new ArrayList<>();

    public int getRoundTime() {
        return roundTime;
    }

    public void setRoundTime(int roundTime) {
        this.roundTime = roundTime;
    }

    public List<JsonScoreEntry> getGestures() {
        return gestures;
    }

    public void setGestures(List<JsonScoreEntry> gestures) {
        this.gestures = gestures;
    }
}
