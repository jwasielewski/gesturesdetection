package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonGesture {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("expected_output")
    @Expose
    private int[] expectedOutput;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int[] getExpectedOutput() {
        return expectedOutput;
    }

    public void setExpectedOutput(int[] expectedOutput) {
        this.expectedOutput = expectedOutput;
    }
}
