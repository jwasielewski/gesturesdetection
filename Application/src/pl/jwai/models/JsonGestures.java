package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class JsonGestures {

    @SerializedName("gestures")
    @Expose
    private List<JsonGesture> gestures = new ArrayList<>();

    public List<JsonGesture> getGestures() {
        return gestures;
    }

    public void setGestures(List<JsonGesture> gestures) {
        this.gestures = gestures;
    }
}
