package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class JsonDataForGesture {

    @SerializedName("input")
    @Expose
    private double[] input;
    @SerializedName("expected_output")
    @Expose
    private double[] expectedOutput;

    public double[] getInput() {
        return input;
    }

    public void setInput(double[] input) {
        this.input = input;
    }

    public double[] getExpectedOutput() {
        return expectedOutput;
    }

    public void setExpectedOutput(double[] expectedOutput) {
        this.expectedOutput = expectedOutput;
    }

}
