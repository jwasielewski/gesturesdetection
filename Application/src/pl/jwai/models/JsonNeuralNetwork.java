package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class JsonNeuralNetwork {

    @SerializedName("number_of_layers")
    @Expose
    private int numberOfLayers;
    @Expose
    private List<JsonNeuron> neurons = new ArrayList<JsonNeuron>();

    public int getNumberOfLayers() {
        return numberOfLayers;
    }

    public void setNumberOfLayers(int numberOfLayers) {
        this.numberOfLayers = numberOfLayers;
    }

    public List<JsonNeuron> getNeurons() {
        return neurons;
    }

    public void setNeurons(List<JsonNeuron> neurons) {
        this.neurons = neurons;
    }

}
