package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class JsonTrainingData {

    @SerializedName("training_data")
    @Expose
    private List<JsonDataForGesture> trainingData = new ArrayList<JsonDataForGesture>();
    @SerializedName("max_epoch")
    @Expose
    private int maxEpoch;
    @SerializedName("learning_rate")
    @Expose
    private double learningRate;
    @SerializedName("momentum")
    @Expose
    private double momentum;
    @SerializedName("error_threshold")
    @Expose
    private double errorThreshold;

    public List<JsonDataForGesture> getTrainingData() {
        return trainingData;
    }

    public void setTrainingData(List<JsonDataForGesture> trainingData) {
        this.trainingData = trainingData;
    }

    public int getMaxEpoch() {
        return maxEpoch;
    }

    public void setMaxEpoch(int maxEpoch) {
        this.maxEpoch = maxEpoch;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getMomentum() {
        return momentum;
    }

    public void setMomentum(double momentum) {
        this.momentum = momentum;
    }

    public double getErrorThreshold() {
        return errorThreshold;
    }

    public void setErrorThreshold(double errorThreshold) {
        this.errorThreshold = errorThreshold;
    }
}