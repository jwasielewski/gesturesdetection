package pl.jwai.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

@Generated("org.jsonschema2pojo")
public class JsonNeuron {

    @Expose
    private int id;
    @SerializedName("layer_id")
    @Expose
    private int layerId;
    @Expose
    private List<JsonSynapse> synapses = new ArrayList<JsonSynapse>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLayerId() {
        return layerId;
    }

    public void setLayerId(int layerId) {
        this.layerId = layerId;
    }

    public List<JsonSynapse> getSynapses() {
        return synapses;
    }

    public void setSynapses(List<JsonSynapse> synapses) {
        this.synapses = synapses;
    }

}
