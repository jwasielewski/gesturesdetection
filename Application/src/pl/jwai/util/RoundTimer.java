package pl.jwai.util;

import pl.jwai.bus.Events;
import pl.jwai.bus.GDEventBus;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RoundTimer {

    private ScheduledExecutorService service;
    private Runnable task;
    private int seconds;

    public RoundTimer(int seconds) {
        this.service = Executors.newSingleThreadScheduledExecutor();
        this.task = createTask();
        this.seconds = seconds;
    }

    public void run() {
        service.schedule(task, seconds, TimeUnit.SECONDS);
    }

    public void shutdown() {
        service.shutdown();
    }

    private Runnable createTask() {
        return () -> GDEventBus.instance().post(Events.endOfRound);
    }

}
