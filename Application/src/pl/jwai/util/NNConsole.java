package pl.jwai.util;

import com.google.common.base.Splitter;
import pl.jwai.neuralnetwork.NeuralNetwork;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class NNConsole {

    public void run() {
        NeuralNetwork network = new NeuralNetwork();
        network.readFromFile("neuralnetwork.json");

        Scanner cin = new Scanner(System.in);
        String line;
        double input[], output[];

        System.out.print("> ");
        while (!(line = cin.nextLine()).isEmpty()) {
            input = getArrayFromInput(line);

            network.setInputs(input);
            output = network.getOutput();

            System.out.print("\n====\n");
            System.out.print("Input:  " + Arrays.toString(input) + "\n");
            System.out.print("Output: " + Arrays.toString(output));
            System.out.print("\n====\n\n> ");
        }

    }

    public double[] getArrayFromInput(String input) {
        List<String> temp = Splitter.on(',').splitToList(input);
        double[] result = new double[temp.size()];
        for (int i = 0; i < temp.size(); ++i) {
            result[i] = Double.valueOf(temp.get(i));
        }
        return result;
    }

}
