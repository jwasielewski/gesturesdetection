package pl.jwai.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class PrettyGson {

    public static Gson gson() {
        return new GsonBuilder().setPrettyPrinting().create();
    }

}
