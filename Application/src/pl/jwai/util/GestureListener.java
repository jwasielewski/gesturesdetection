package pl.jwai.util;

public interface GestureListener {
    void onGestureStart();

    void onGestureEnd();
}
