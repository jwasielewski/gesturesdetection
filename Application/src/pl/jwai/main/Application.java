package pl.jwai.main;

import com.google.gson.Gson;
import pl.jwai.neuralnetwork.NeuralNetworkCreator;
import pl.jwai.gui.MainWindow;
import pl.jwai.models.JsonAppConfiguration;
import pl.jwai.neuralnetwork.Backpropagator;
import pl.jwai.util.NNConsole;
import pl.jwai.util.PrettyGson;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class Application {

    public static void main(String... args) {

        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("-create")) {
                NeuralNetworkCreator creator = new NeuralNetworkCreator();
                creator.run();
            } else if (args[0].equalsIgnoreCase("-backpropagate") && args.length >= 2) {
                Backpropagator backpropagator = new Backpropagator();
                backpropagator.loadData(args[1]);
                backpropagator.run();
            } else if (args[0].equalsIgnoreCase("-manual")) {
                NNConsole console = new NNConsole();
                console.run();
            } else {
                runMainProgram();
            }
        } else {
            runMainProgram();
        }

    }

    public static void runMainProgram() {
        try {
            SwingUtilities.invokeAndWait(() -> {
                MainWindow mainWindow = new MainWindow(readAppConfiguration());
                mainWindow.setVisible(true);
            });
        } catch (InterruptedException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static JsonAppConfiguration readAppConfiguration() {
        Gson gson = PrettyGson.gson();
        JsonAppConfiguration configuration = null;

        try {
            try (BufferedReader reader = new BufferedReader(new FileReader("configuration.json"))) {
                configuration = gson.fromJson(reader, JsonAppConfiguration.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        return configuration;
    }

}
