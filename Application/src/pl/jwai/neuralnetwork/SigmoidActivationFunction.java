package pl.jwai.neuralnetwork;

public class SigmoidActivationFunction {

    public static double activate(double sumOfWeights) {
        //return 1.0 / (1 + Math.exp(-1.0 * sumOfWeights));
        double e = Math.exp(-1.0 * sumOfWeights);
        return (1 - e)/(1 + e);
    }

    public static double derivative(double sumOfWeights) {
        //return sumOfWeights * (1.0 - sumOfWeights);
        return (1 - sumOfWeights * sumOfWeights) / 2;
    }

}
