package pl.jwai.neuralnetwork;

import java.util.List;

public class NeuralNetworkConfiguration {

    private String fileName;
    private int numberOfLayers;
    private List<Integer> layers;

    public NeuralNetworkConfiguration(String fileName, int numberOfLayers, List<Integer> layers) {
        this.fileName = fileName;
        this.numberOfLayers = numberOfLayers;
        this.layers = layers;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getNumberOfLayers() {
        return numberOfLayers;
    }

    public void setNumberOfLayers(int numberOfLayers) {
        this.numberOfLayers = numberOfLayers;
    }

    public List<Integer> getLayers() {
        return layers;
    }

    public void setLayers(List<Integer> layers) {
        this.layers = layers;
    }
}
