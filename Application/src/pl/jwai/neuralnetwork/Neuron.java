package pl.jwai.neuralnetwork;

import pl.jwai.models.JsonNeuron;
import pl.jwai.models.JsonSynapse;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Neuron {

    private int id;
    private int layerId;
    private List<Synapse> synapses;

    private double output;
    private double derivative;
    private double weightedSum;
    private double error;

    private NeuralNetwork parent;

    public Neuron(int id, int layerId, NeuralNetwork parent) {
        this.id = id;
        this.layerId = layerId;
        output = derivative = 0;
        synapses = new LinkedList<>();
        this.parent = parent;
    }

    public void addInput(Synapse synapse) {
        synapses.add(synapse);
    }

    public List<Synapse> getInputs() {
        return new LinkedList<>(synapses);
    }

    public double getSynapseWeightForNeuron(int id) {
        for (Synapse synapse : synapses) {
            if (synapse.getNeuronId() == id) {
                return synapse.getWeight();
            }
        }
        return 0.0;
    }

    public boolean hasOutputFromNeuron(int id) {
        return synapses.stream().filter(synapse -> synapse.getNeuronId() == id).collect(Collectors.toList()).size() > 0;
    }

    private void calculate() {
        weightedSum = 0;
        synapses.forEach(e -> weightedSum += e.getWeight() * parent.getNeuronById(e.getNeuronId()).getOutput());
    }

    public void activate() {
        calculate();
        output = SigmoidActivationFunction.activate(weightedSum);
        derivative = SigmoidActivationFunction.derivative(output);
    }

    public void setRandomWeights() {
        synapses.stream().forEach(synapse -> synapse.setWeight(WageRandomizer.getInstance().getRandomWeight()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLayerId() {
        return layerId;
    }

    public void setLayerId(int layerId) {
        this.layerId = layerId;
    }

    public double getOutput() {
        // input layer || bias
        if (layerId == 0 || id == -1) {
            return output;
        }

        activate();
        return output;
    }

    public void setOutput(double output) {
        this.output = output;
    }

    public double getDerivative() {
        return derivative;
    }

    public void setDerivative(double derivative) {
        this.derivative = derivative;
    }

    public double getError() {
        return error;
    }

    public void setError(double error) {
        this.error = error;
    }

    public void setParent(NeuralNetwork parent) {
        this.parent = parent;
    }

    public JsonNeuron toJson() {
        JsonNeuron jsonNeuron = new JsonNeuron();

        jsonNeuron.setId(id);
        jsonNeuron.setLayerId(layerId);

        List<JsonSynapse> jsonSynapses = new ArrayList<>();
        synapses.stream().forEach(synapse -> jsonSynapses.add(new JsonSynapse(synapse.getNeuronId(), synapse.getWeight())));

        jsonNeuron.setSynapses(jsonSynapses);

        return jsonNeuron;
    }
}
