package pl.jwai.neuralnetwork;

public class Synapse {

    private int neuronId;
    private double weight;

    public Synapse() {
    }

    public Synapse(int neuronId) {
        this(neuronId, 0);
    }

    public Synapse(int neuronId, double weight) {
        this.neuronId = neuronId;
        this.weight = weight;
    }

    public int getNeuronId() {
        return neuronId;
    }

    public void setNeuronId(int neuronId) {
        this.neuronId = neuronId;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}
