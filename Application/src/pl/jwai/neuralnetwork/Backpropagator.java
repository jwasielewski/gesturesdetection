package pl.jwai.neuralnetwork;

import com.google.gson.Gson;
import pl.jwai.models.JsonDataForGesture;
import pl.jwai.models.JsonTrainingData;
import pl.jwai.util.PrettyGson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Backpropagator {

    private NeuralNetwork network;
    private JsonTrainingData trainingData;
    private Gson gson;

    private Map<String, Double> synapseDeltaCache;

    public Backpropagator() {
        this.gson = PrettyGson.gson();
    }

    public void loadData(String fileWithTrainingData) {
        try {
            try (BufferedReader reader = new BufferedReader(new FileReader(fileWithTrainingData))) {
                trainingData = gson.fromJson(reader, JsonTrainingData.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void run() {
        network = new NeuralNetwork();
        network.readFromFile("neuralnetwork.json");

        System.out.println("=== Rozpoczynam uczenie sieci ===");
        int currentEpoch = 0, globalSize;
        double globalError;
        do {
            globalError = 0.0;
            globalSize = 0;

            List<JsonDataForGesture> jsonDataForGestureList = trainingData.getTrainingData();
            Collections.shuffle(jsonDataForGestureList, new Random(System.nanoTime()));

            System.out.print(" Epoka " + (currentEpoch + 1) + "...\t\t");
            for(JsonDataForGesture data : jsonDataForGestureList) {
                globalSize += data.getInput().length;
                globalError += backpropagate(data.getInput(), data.getExpectedOutput());
            }
            ++currentEpoch;
            // Root Mean Square (RMS) Error
            // http://www.heatonresearch.com/node/698
            globalError = Math.sqrt(globalError / globalSize);
            System.out.println("Error = " + globalError);
        } while (globalError >= trainingData.getErrorThreshold() ||  currentEpoch < trainingData.getMaxEpoch());
        System.out.println("=== Zakonczylem uczenie sieci ===");
        try {
            network.writeToFile("neuralnetwork.json");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private double backpropagate(double[] input, double[] expected) {
        double error;
        synapseDeltaCache = new HashMap<>();

        network.setInputs(input);
        double[] output = network.getOutput();

        // pierwsza faza
        for (int i = network.getNumberOfLayers() - 1; i > 0; --i) {
            List<Neuron> layer = network.getNeuronsInLayer(i);
            for (int j = 0; j < layer.size(); ++j) {
                Neuron neuron = layer.get(j);
                double neuronError;
                if (neuron.getLayerId() == network.getNumberOfLayers() - 1) {
                    neuronError = neuron.getDerivative() * (expected[j] - output[j]);
                } else {
                    double sum = 0.0;
                    List<Neuron> nextLayer = network.getNeuronsInLayer(j + 1);
                    for (Neuron neuronInNextLayer : nextLayer) {
                        sum += neuronInNextLayer.getError() * neuronInNextLayer.getSynapseWeightForNeuron(neuron.getId());

                        // tworzenie czystego cache'a
                        storeDeltaInCache(neuronInNextLayer.getId(), neuron.getId(), 0);
                    }
                    neuronError = sum * neuron.getDerivative();
                }
                neuron.setError(neuronError);
            }
        }

        // druga faza
        for (int i = network.getNumberOfLayers() - 1; i > 0; --i) {
            List<Neuron> layer = network.getNeuronsInLayer(i);
            for (Neuron neuron : layer) {
                for (Synapse synapse : neuron.getInputs()) {
                    double change = neuron.getError() * network.getNeuronById(synapse.getNeuronId()).getOutput();
                    double newWeight = synapse.getWeight() + trainingData.getLearningRate() * change +
                            trainingData.getMomentum() * getSynapseLastDeltaFromCache(neuron.getId(),
                                    synapse.getNeuronId());
                    synapse.setWeight(newWeight);
                    storeDeltaInCache(neuron.getId(), synapse.getNeuronId(), newWeight);
                }
            }
        }

        output = network.getOutput();
        error = calculateNeuralNetworkError(output, expected);
        return error;
    }

    private double calculateNeuralNetworkError(double[] actual, double[] expected) {
        double error = 0.0;

        if (actual.length != expected.length) {
            throw new IllegalArgumentException("Ilość aktualnych i oczekiwanych wyników powinna być taka sama");
        }

        for (int i = 0; i < expected.length; ++i) {
            error += Math.pow(expected[i] - actual[i], 2);
        }

        return error;
    }

    private void storeDeltaInCache(int from, int to, double delta) {
        synapseDeltaCache.put(getSynapseIdForMap(from, to), delta);
    }

    private double getSynapseLastDeltaFromCache(int from, int to) {
        Double result = synapseDeltaCache.get(getSynapseIdForMap(from, to));
        if (result == null) {
            return 0.0;
        } else {
            return result;
        }
    }

    private String getSynapseIdForMap(int fromNeuronId, int toNeuronId) {
        return String.format("%d,%d", fromNeuronId, toNeuronId);
    }

}
