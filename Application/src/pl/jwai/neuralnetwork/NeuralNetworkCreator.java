package pl.jwai.neuralnetwork;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class NeuralNetworkCreator {

    public void run() {
        int numerOfLayers;
        List<Integer> layers = new LinkedList<>();

        Scanner cin = new Scanner(System.in);

        System.out.println("=== KREATOR NOWEJ SIECI NEURONOWEJ ===");

        System.out.print("Liczba warstw: ");
        numerOfLayers = cin.nextInt();

        cin.nextLine();

        int numberOfNeurons;
        for (int i = 0; i < numerOfLayers; ++i) {
            if (i == 0) { // warstwa wejsciowa
                System.out.print("Ilosc neuronow w warstwie wejsciowej: ");
            } else if (i == numerOfLayers - 1) { // warstwa wyjsciowa
                System.out.print("Ilosc neuronow w warstwie wyjsciowej: ");
            } else {
                System.out.print(String.format("Ilosc neuronow w %d. warstwie ukrytej: ", i));
            }
            numberOfNeurons = cin.nextInt();
            layers.add(numberOfNeurons);

            cin.nextLine();

            if (i == 0) {
                WageRandomizer.newInstance(numberOfNeurons);
            }
        }

        System.out.println("Prosze czekac...");
        try {
            NeuralNetworkConfiguration configuration = new NeuralNetworkConfiguration("neuralnetwork.json",
                    numerOfLayers, new LinkedList<>(layers));
            create(configuration);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(-1);
        }
        System.out.println("Nowa siec zostala utworzona");
    }

    private void create(NeuralNetworkConfiguration configuration) throws IOException {
        NeuralNetwork network = new NeuralNetwork();
        network.createNewNeuralNetwork(configuration);
        network.writeToFile(configuration.getFileName());
    }

}
