package pl.jwai.neuralnetwork;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.jwai.models.JsonNeuralNetwork;
import pl.jwai.util.PrettyGson;

import java.io.*;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class NeuralNetwork {
    static final Logger log = LoggerFactory.getLogger(NeuralNetwork.class);

    private List<Neuron> neurons;
    private Neuron bias;
    private int numberOfLayers;

    public NeuralNetwork() {
        numberOfLayers = 0;
        neurons = new LinkedList<>();

        bias = new Neuron(-1, -1, this);
        bias.setOutput(1.0);
    }

    public void setInputs(double[] inputs) {
        List<Neuron> inputNeurons = neurons.stream().filter(neuron -> neuron.getLayerId() == 0)
                .collect(Collectors.toList());

        inputNeurons.sort(new Comparator<Neuron>() {
            @Override
            public int compare(Neuron o1, Neuron o2) {
                return o1.getId() - o2.getId();
            }
        });

        if (inputNeurons.size() != inputs.length) {
            throw new IllegalArgumentException("Liczba wejsc sieci jest rozna od liczby przekazanych wartosci!");
        }

        for (int i = 0; i < inputNeurons.size(); ++i) {
            inputNeurons.get(i).setOutput(inputs[i]);
            // System.out.println(inputNeurons.get(i).getId());
        }
    }

    public double[] getOutput() {
        List<Neuron> outputNeurons = neurons.stream().filter(neuron -> neuron.getLayerId() == numberOfLayers - 1)
                .collect(Collectors.toList());

//        outputNeurons.sort(new Comparator<Neuron>() {
//            @Override
//            public int compare(Neuron o1, Neuron o2) {
//                return o1.getId() - o2.getId();
//            }
//        });

        double[] output = new double[outputNeurons.size()];

        for (int i = 0; i < outputNeurons.size(); ++i) {
            output[i] = outputNeurons.get(i).getOutput();
        }

        return output;
    }

    public List<Integer> getNeuronsIdsInLayer(int layerId) {
        if (layerId < 0 || layerId > numberOfLayers) {
            return new LinkedList<>();
        }
        return neurons.stream().filter(neuron -> neuron.getLayerId() == layerId).map(Neuron::getId)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public List<Neuron> getNeuronsInLayer(int layerId) {
        if (layerId < 0 || layerId > numberOfLayers) {
            return new LinkedList<>();
        }
        return neurons.stream().filter(neuron -> neuron.getLayerId() == layerId)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    public Neuron getNeuronById(int id) {
        if (id == -1) {
            return bias;
        } else {
            return neurons.stream().filter(neuron -> neuron.getId() == id).findFirst().get();
        }
    }

    public List<Neuron> getOutputsFromNeuronByIdInLayer(int id, int layer) {
        return getNeuronsInLayer(layer).stream().filter(neuron -> neuron.hasOutputFromNeuron(id))
                .collect(Collectors.toList());
    }

    public int getNumberOfLayers() {
        return numberOfLayers;
    }

    public void createNewNeuralNetwork(NeuralNetworkConfiguration configuration) {
        int neuronId = 0;

        numberOfLayers = configuration.getNumberOfLayers();

        //log.debug("start => creating neurons");
        // utworzenie neuronów
        for (int i = 0; i < numberOfLayers; ++i) {
            log.debug("layer:'{}', neurons in layer:'{}'", i, configuration.getLayers().get(i));
            for (int j = 0; j < configuration.getLayers().get(i); ++j) {
                neurons.add(new Neuron(neuronId++, i, this));
            }
        }
        //log.debug("end => creating neurons");

        //log.debug("neurons.size()='{}'", neurons.size());

        //log.debug("start => creating synapses");
        // utworzenie połączeń pomiędzy neuronami
        // połącznie tworzone są od końca
        for (int i = numberOfLayers - 1; i > 0; --i) {
            List<Neuron> neurons = getNeuronsInLayer(i);
            List<Neuron> previousNeurons = getNeuronsInLayer(i - 1);

            for (Neuron neuron : neurons) {
                neuron.addInput(new Synapse(bias.getId()));
                for (Neuron previousNeuron : previousNeurons) {
                    neuron.addInput(new Synapse(previousNeuron.getId()));
                }
            }
        }
        //log.debug("end => creating synapses");

        //log.debug("start => creating random wages in synapses");
        neurons.stream()
                .filter(neuron -> neuron.getLayerId() != 0) // 0 to id warstwy wejsciowej
                .collect(Collectors.toCollection(LinkedList::new))
                .forEach(Neuron::setRandomWeights);
        //log.debug("end => creating random wages in synapses");
    }

    public void writeToFile(String fileName) throws IOException {
        JsonNeuralNetwork jsonNeuralNetwork = new JsonNeuralNetwork();

        jsonNeuralNetwork.setNumberOfLayers(numberOfLayers);
        jsonNeuralNetwork.setNeurons(neurons.stream().map(Neuron::toJson).collect(Collectors.toList()));

        Gson gson = PrettyGson.gson();
        String json = gson.toJson(jsonNeuralNetwork);

        try (FileOutputStream outputStream = new FileOutputStream(new File(fileName))) {
            outputStream.write(json.getBytes());
        }
    }

    public void readFromFile(String fileName) {
        Gson gson = PrettyGson.gson();
        JsonNeuralNetwork jsonNeuralNetwork = null;

        try {
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
                jsonNeuralNetwork = gson.fromJson(reader, JsonNeuralNetwork.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        numberOfLayers = jsonNeuralNetwork.getNumberOfLayers();
        jsonNeuralNetwork.getNeurons().stream().forEach(jsonNeuron -> {
            Neuron neuron = new Neuron(jsonNeuron.getId(), jsonNeuron.getLayerId(), this);

            jsonNeuron.getSynapses().stream()
                    .forEach(jsonSynapse -> neuron.addInput(new Synapse(jsonSynapse.getId(), jsonSynapse.getWeight())));

            neurons.add(neuron);
        });
    }
}
