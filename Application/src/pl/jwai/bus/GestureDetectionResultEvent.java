package pl.jwai.bus;

public class GestureDetectionResultEvent {
    private String mDetectedGestureName;
    private int mPoints;

    public GestureDetectionResultEvent(String name, int points) {
        mDetectedGestureName = name;
        mPoints = points;
    }

    public int getPoints() {
        return mPoints;
    }

    @Override
    public String toString() {
        return String.format("%s, +%d", mDetectedGestureName, mPoints);
    }
}
