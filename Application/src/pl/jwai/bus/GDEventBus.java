package pl.jwai.bus;

import com.google.common.eventbus.EventBus;

public class GDEventBus {
    private static GDEventBus mInstance;

    private EventBus mBus;

    private GDEventBus() {
        mBus = new EventBus();
    }

    public static GDEventBus instance() {
        if (mInstance == null) {
            mInstance = new GDEventBus();
        }
        return mInstance;
    }

    public void register(Object who) {
        mBus.register(who);
    }

    public void post(Object what) {
        mBus.post(what);
    }
}
